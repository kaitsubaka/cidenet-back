package utils

import (
	"cidenet/constants"
	"cidenet/models"
	"fmt"
	"sync"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// Database => represents the connection to database instance, is a singleton.
type Database struct {
	Connection *gorm.DB
}

var (
	// concurrency stuff
	once sync.Once
	// database singleton instance
	databaseInstance *Database
)

// GetDataBaseInstance => return the instance of data base if initialized
// or initialize one instead
func GetDataBaseInstance() (*Database, error) {
	// global function error
	var err error
	// call to init function one and only one time even if GetDataBaseInstance is called multiple times
	once.Do(func() {
		var db *gorm.DB
		// create the new Database object
		databaseInstance = new(Database)
		// open the sequelite connection
		db, err = gorm.Open(sqlite.Open(fmt.Sprint(constants.DBPATH, "/", constants.DBNAME)), &gorm.Config{})
		// Assign the db connection to Conection
		databaseInstance.Connection = db
		// Migrate models
		databaseInstance.Connection.AutoMigrate(&models.Employee{})
	})
	// elevates the error if connection could not be opened
	// then the instance returned is nil
	if err != nil {
		return nil, err
	}
	// return the databaseInstance and error as nil
	return databaseInstance, nil
}

// Paginate => scope for pagination
func Paginate(page, pageSize int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		// default page bc offset never is 0
		if page == 0 {
			page = 1
		}
		// limit the page size just 4 security and not burn the value in code.
		switch {
		case pageSize > 100:
			pageSize = 100
		case pageSize <= 0:
			pageSize = 10
		}
		// calculates offset for pagination
		offset := (page - 1) * pageSize
		// return the pagination scope
		return db.Offset(offset).Limit(pageSize)
	}
}

// Paginate => scope for pagination
func FilterEmployees(filters ...string) func(db *gorm.DB) *gorm.DB {
	for i, value := range filters {
		filters[i] = valueToFilterParam(value)
	}
	return func(db *gorm.DB) *gorm.DB {
		// return the filter scope
		return db.Where(
			"name LIKE ? AND other_names LIKE ? "+
				"AND first_surname LIKE ? AND second_surname LIKE ? "+
				"AND identity_doc_type LIKE ? AND identity_doc_number LIKE ? "+
				"AND country_of_employment LIKE ? AND email LIKE ? "+
				"AND state LIKE ?",
			filters[0],
			filters[1],
			filters[2],
			filters[3],
			filters[4],
			filters[5],
			filters[6],
			filters[7],
			filters[8],
		)
	}
}

func valueToFilterParam(value string) (result string) {
	result = "%" + value + "%"
	return
}
