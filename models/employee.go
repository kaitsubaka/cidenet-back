package models

import (
	"time"

	"gorm.io/gorm"
)

// Employee => represents the employee at application level in this service.
// here are just basic constrains because all validations will be performed
// in the flutter application.
// note: single table patter bc i dont like read only collections in simple things.
type Employee struct {
	gorm.Model
	FirstSurname        string    `json:"firstSurname" gorm:"index:idx_first_surname; type:VARCHAR(20); not null"`
	SecondSurname       string    `json:"secondSurname" gorm:"index:idx_second_surname; type:VARCHAR(20); not null"`
	Name                string    `json:"name" gorm:"index:idx_name; type:VARCHAR(20); not null"`
	OtherNames          string    `json:"otherNames" gorm:"index:idx_orher_names; type:VARCHAR(20); not null"`
	CountryOfEmployment string    `json:"countryOfEmployment" gorm:"index:idx_country; type:VARCHAR(2); not null"`
	IdentityDocType     string    `json:"identityDocType" gorm:"uniqueIndex:idx_identity_info;type:VARCHAR(2); not null"`
	IdentityDocNumber   string    `json:"identityDocNumber" gorm:"uniqueIndex:idx_identity_info; type:VARCHAR(20); not null"`
	Email               string    `json:"email" gorm:"index:idx_email; unique; type:VARCHAR(300); not null"`
	Area                string    `json:"area" gorm:"; type:VARCHAR(20); not null"`
	State               string    `json:"state" gorm:"<-:create; index:idx_state; default:'Activo'; not null"`
	AdmissionDate       time.Time `json:"admissionDate" gorm:"not null"`
}
