package controllers

import (
	"cidenet/models"
	"cidenet/utils"
	"net/http"
	"strconv"
	"strings"
	"unicode"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

// GetEmployees => get all employees and filter it by page
func GetEmployees(c echo.Context) error {
	// get the database instance connection
	database, err := utils.GetDataBaseInstance()
	// handle conection error
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}
	// allocate in memory the result set of employees
	employees := make([]*models.Employee, 0)
	// code inside other scope for security and management in transaction
	{
		// get params id
		page, err := strconv.Atoi(c.QueryParam("page"))
		// err if there it is invalid at parse then default 0
		if err != nil {
			page = 0
		}
		// get params id
		pageSize, err := strconv.Atoi(c.QueryParam("page_size"))
		// err if there it is invalid at parse then default 10
		if err != nil {
			pageSize = 10
		}
		// get all users transaction
		database.Connection.Model(&models.Employee{}).Scopes(utils.FilterEmployees(
			c.QueryParam("name"),
			c.QueryParam("other_names"),
			c.QueryParam("first_surname"),
			c.QueryParam("second_surname"),
			c.QueryParam("identity_doc_type"),
			c.QueryParam("identity_doc_number"),
			c.QueryParam("country_of_employment"),
			c.QueryParam("email"),
			c.QueryParam("state"),
		),
		).Scopes(utils.Paginate(page, pageSize)).Find(&employees)
	}
	// return result set of employees and status code
	return c.JSON(http.StatusOK, employees)
}

// CreateEmployee => creates an employee in the database and returns its values
func CreateEmployee(c echo.Context) error {
	// get the database instance connection
	database, err := utils.GetDataBaseInstance()
	// handle conection error
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}
	// allocate in memory the result employee
	newEmployee := new(models.Employee)
	// code inside other scope for security and management in transaction
	{
		// extract the values of body into newEmployee
		if err := c.Bind(newEmployee); err != nil {
			return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		}
		baseName := newEmployee.Name + "." + newEmployee.FirstSurname
		baseDomine := "@cidenet.com.co"
		// create employee transaction
		database.Connection.Transaction(func(tx *gorm.DB) error {
			var count int64
			tx.Unscoped().Model(&models.Employee{}).Where("email LIKE ?", "%"+baseName+"%").Count(&count)
			// do some database operations in the transaction (use 'tx' from this point, not 'db')
			if count > 0 {
				baseName = baseName + "." + strconv.Itoa(int(count))
			}
			newEmployee.ID = 0
			newEmployee.Email = SpaceMap(baseName + baseDomine)
			if err := tx.Create(newEmployee).Error; err != nil {
				// return any error will rollback
				return c.JSON(http.StatusBadRequest, err)
			}
			// return nil will commit the whole transaction
			return nil
		})
	}
	// return the new employee and status code
	return c.JSON(http.StatusCreated, newEmployee)
}

// UpdateEmployee => updates an employee in the database and returns its values
func UpdateEmployee(c echo.Context) error {
	// get the database instance connection
	database, err := utils.GetDataBaseInstance()
	// handle conection error
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}
	// allocate in memory the param employee
	newEmployee := new(models.Employee)
	// current value in database for returns
	currentEmployee := new(models.Employee)
	// code inside other scope for security and management in transaction
	{
		// get params id
		id, err := strconv.Atoi(c.Param("id"))
		// err if there it is invalid at parse
		if err != nil {
			return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		}

		// extract the values of body into newEmployee
		if err := c.Bind(newEmployee); err != nil {
			return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		}
		// update employee transaction

		err = database.Connection.Transaction(func(tx *gorm.DB) error {

			tx.First(&currentEmployee, id)
			// do some database operations in the transaction (use 'tx' from this point, not 'db')
			if newEmployee.Name != "" || newEmployee.FirstSurname != "" {
				baseName := ""
				baseDomine := "@cidenet.com.co"
				if currentEmployee.Name != newEmployee.Name && newEmployee.Name != "" {
					baseName = newEmployee.Name + "."
				} else {
					baseName = currentEmployee.Name + "."
				}
				if currentEmployee.FirstSurname != newEmployee.FirstSurname && newEmployee.FirstSurname != "" {
					baseName = SpaceMap(baseName + newEmployee.FirstSurname)
				} else {
					baseName = SpaceMap(baseName + currentEmployee.FirstSurname)
				}
				newEmployee.Email = baseName + baseDomine

			}
			result := tx.Model(
				&currentEmployee,
			).Clauses(
				clause.Returning{},
			).Updates(newEmployee)
			if err := result.Error; err != nil {
				return err
			}
			// return nil will commit the whole transaction
			return nil
		})
		if err != nil {
			return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		}
	}
	// return the new employee information and status code
	return c.JSON(http.StatusOK, currentEmployee)
}

// DeleteEmployee => deletes an employee in the database
func DeleteEmployee(c echo.Context) error {
	// get the database instance connection
	database, err := utils.GetDataBaseInstance()
	// handle conection error
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}
	// code inside other scope for security and management in transaction
	{
		// get params id
		id, err := strconv.Atoi(c.Param("id"))
		// err if there it is invalid at parse
		if err != nil {
			return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		}
		// delete employee transaction
		database.Connection.Delete(&models.Employee{}, id)
	}
	// return the status code
	return c.NoContent(http.StatusNoContent)
}
func SpaceMap(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, str)
}
