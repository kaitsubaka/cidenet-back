# Cidenet Back


**This project is for read only purpouses, if you want to use it contact the owner first.**

To run it, just pull the dependencies with **go mod download** and then execute with **go run server.go** or build executable **go build** that creates the executable **cidenet.{out,exe,o}**  

If you want a test db, rename **cidenet.bk** to **cidenet.db** in */database*. 

To clean db just erase **cidenet.db** from the file system.