package constants

const (
	// DBNAME => name of database
	DBNAME = "cidenet.db"
	// DBPATH => path to db folder
	DBPATH = "database"
)
