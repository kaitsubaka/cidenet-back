package routes

import (
	"cidenet/controllers"

	"github.com/labstack/echo/v4"
)

// UseEmployeeRouter => creates all routes and sub routes for employees
func UseEmployeeRouter(router *echo.Group) {
	router.GET("/", controllers.GetEmployees)
	// do: get all employees in the system
	// at: /employees/ or /employees

	router.POST("/", controllers.CreateEmployee)
	// do: create a new employee in the system
	// at: /employees/ or /employees
	// payload: employee

	router.PUT("/:id/", controllers.UpdateEmployee)
	// do: updates the employee information
	// at: /employees/{id}/ or /employees/{id}
	// payload: employee

	router.DELETE("/:id/", controllers.DeleteEmployee)
	// do: delete the employee information in the system
	// at: /employees/{id}/ or /employees/{id}
}
