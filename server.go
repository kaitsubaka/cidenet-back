package main

import (
	"cidenet/routes"
	"cidenet/utils"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// new echo app
	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))
	e.Pre(middleware.AddTrailingSlash())
	// init database singleton before adding the routes
	utils.GetDataBaseInstance()
	// Routes => Groups
	v1 := e.Group("/api/v1")
	{
		// employees routes
		routes.UseEmployeeRouter(v1.Group("/employees"))
	}
	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}
